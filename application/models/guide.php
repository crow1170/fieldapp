<?php
class Guide extends CI_Model {
  function __construct(){
    parent::__construct();
  }
  function g($a,$b=array(),$c=0){$d=$this->db->get_where($a,$b);$e=$d->result_array();if($c)return $e[0];return $e;}
  function u($a,$b=array(),$c){$this->db->update($a,$c,$b);return $this->db->affected_rows();}
  function i($a,$b){$this->db->insert($a,$b);return $this->db->insert_id();}
  function d($a,$b=array()){$this->db->delete($a,$b);return $this->db->affected_rows();}
  function e($a){$b=$this->db->query($a);if(is_object($b))return $b->result_array();return $b;}
}
?>
