  <div data-role="collapsible">
    <h2>Projects</h2>
    <form>
      <input data-type="search" id="divOfPs-input">
    </form>
    <div class="elements" data-filter="true" data-input="#divOfPs-input">
    <?php
      if(empty($projects)){ ?>
        <p><em>There are currently no projects for this client...</em></p>
      <?php }else{ ?>
        <?php foreach($projects as $project){ ?>
          <a style="white-space:normal;overflow:visible;" data-transition="flip" class="ui-btn ui-corner-all ui-shadow ui-btn-inline" href="<?=site_url('project/id').'/'.$project['projId'];?>"><?=$project['name'];?></a>
        <?php } ?>
    <?php } ?>
    </div>
  </div>
