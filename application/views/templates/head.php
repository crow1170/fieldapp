<?php
  // Init
  $clientHot = '';
  $projHot = '';
  $apptHot = '';
  if($head['tab'] == 'client'){
    $clientHot = 'class="ui-btn-active"'; 
  }
  if($head['tab'] == 'proj'){
    $projHot = 'class="ui-btn-active"'; 
  }
  if($head['tab'] == 'appt'){
    $apptHot = 'class="ui-btn-active"'; 
  }
?>
<!doctype html>
<html>
<head>
  <!-- Meta -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <!-- js -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/jquerymobile/1.4.2/jquery.mobile.min.js"></script>

  <!-- CSS -->
  <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jquerymobile/1.4.2/jquery.mobile.min.css" />
  <link rel="stylesheet" href="//crow1170.com/fieldapp/assets/css/jquery.mobile.icons.min.css" />
  <link rel="stylesheet" href="//crow1170.com/fieldapp/assets/css/Bootstrap.min.css" />
  <style>
  </style>
</head>
<body>
  <div data-role="page">
    <div data-role="header" data-position="inline">
      <div data-role="navbar">
        <ul>
          <li><a href="<?=site_url('client');?>" data-icon="phone" <?=$clientHot;?>>Clients</a></li>
          <li><a href="<?=site_url('project');?>" data-icon="bars" <?=$projHot;?>>Projects</a></li>
          <li><a href="<?=site_url('appointment');?>" data-icon="location" <?=$apptHot;?>>Appt.s</a></li>
        </ul>
      </div><!--/navbar-->
    </div><!--/header-->
