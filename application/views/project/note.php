<div data-role="content">
  <form action="<?=current_url();?>" method="POST">
    <?php if(validation_errors()){?>
      <a href="#" data-role="button" data-theme="c" data-icon="alert">Note content is required</a>
    <?php }else{ ?>
      <a href="#" data-role="button" data-theme="d" data-icon="edit">Creating New Note on <?=$parent['name'];?></a>
    <?php } ?>
    <label for="desc" class="ui-hidden-accessible">Note:</label>
    <textarea type="text" name="desc" id="desc" placeholder="Description"/></textarea>
    <input type="submit" data-icon="comment" value="Create New Note"/>
  </form>
</div><!--/content-->
