<?php
  // Init
?>
<div data-role="content">
  <a data-theme="a" href="<?=site_url('client/id').'/'.$project['clientId'];?>" data-role="button" data-icon="carat-l">Back to Client</a>
  <h1><?=$project['name'];?></h1>
  <p><?=$project['desc'];?></p>
  <a data-theme="d" href="<?=site_url('appointment/create').'/42';?>" data-role="button" data-icon="plus">New Appointment</a>
  <a data-theme="b" data-role="button" data-icon="plus" href="<?=site_url('project/note').'/'.$project['projId'];?>">New Note</a>
  <br />
  <div data-role="collapsible">
  <h2>Appointments</h2>
  <script>
    function timeToString(input){
      var timestamp = new Date(input);
      return timestamp.toDateString() + ' ' + timestamp.toLocaleTimeString();
    }
  </script>
  <form class="ui-filterable"><input placeholder="Search..." id="filterBasic-input" data-type="search"></form>
  <ul  class="ui-listview" data-role="listview" data-inset="true" data-filter="true" data-input="#filterBasic-input">
    <?php foreach($appts as $appt){ ?>
    <li>
      <a data-transition="flip" class="ui-btn ui-corner-all ui-shadow ui-btn-inline" href="<?=site_url('appointment/id').'/'.$appt['apptId'];?>">
       <?=date('D, F jS', strtotime($appt['date']));?>
      </a>
    </li>
    <?php } ?>
  </ul>
  </div>
  <?=$notesHTML;?>
</div><!--/content-->
