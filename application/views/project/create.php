<div data-role="content">
  <form action="<?=current_url();?>" method="POST">
    <?php if(validation_errors()){?>
      <a href="#" data-role="button" data-theme="c" data-icon="alert">Project Name is required</a>
    <?php }else{ ?>
      <a href="#" data-role="button" data-theme="d" data-icon="edit">Creating New Project</a>
    <?php } ?>
    <label for="clientId" class="ui-hidden-accessible">Client</label>
    <select name="clientId" id="clientId">
      <?php foreach($clients as $client){ ?>
        <option value="<?=$client['clientId'];?>"><?=$client['name'];?></option>
      <?php } ?>
    </select>
    <label for="projectname" class="ui-hidden-accessible">Project Name:</label>
    <input type="text" name="projectname" id="projectname" placeholder="Project Name" value=""/>
    <label for="desc" class="ui-hidden-accessible">Project Description:</label>
    <textarea type="text" name="desc" id="desc" placeholder="Description"/></textarea>
    <input type="submit" data-icon="bullets" value="Create New Project"/>
  </form>
</div><!--/content-->
