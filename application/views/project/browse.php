<?php
  // Init
?>
<div data-role="content">
  <a href="<?=site_url('project/create');?>" data-role="button" data-theme="d" data-icon="plus">New Project</a>
<form class="ui-filterable"><input placeholder="Search..." id="filterBasic-input" data-type="search"></form>
<ul  class="ui-listview" data-role="listview" data-inset="true" data-filter="true" data-input="#filterBasic-input">
  <?php
    foreach($projects as $proj){ ?>
      <li><a data-transition="flip" class="ui-btn ui-corner-all ui-shadow ui-btn-inline" href="<?=site_url('project/id').'/'.$proj['projId'];?>"><?=$proj['name'];?></a></li>
    <?php }
  ?>
</ul>
</div><!--/content-->
