<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
if ($login_by_username AND $login_by_email) {
	$login_label = 'Email or login';
} else if ($login_by_username) {
	$login_label = 'Login';
} else {
	$login_label = 'Email';
}
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'size'	=> 30,
);
$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
	'value'	=> 1,
	'checked'	=> set_value('remember'),
	'style' => 'margin:0;padding:0',
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
);
?>


<!doctype html>
<html ng-app>
<head>
  <!-- Meta -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <!-- js -->
  <script async src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/jquerymobile/1.4.2/jquery.mobile.min.js"></script>
  <script>
    function HeadCtrl($scope){
      $scope.debug = "HeadCtrl loaded.";
    }
  </script>

  <!-- CSS -->
  <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jquerymobile/1.4.2/jquery.mobile.min.css" />
  <style>
  </style>
</head>
<body>
  <div data-role="page" data-theme="a">
    <div data-role="header" data-position="inline">
      <h1>BMC FieldApp</h1>
      <div data-role="navbar">
        <ul>
          <li><a href="http://crow1170.com/fieldapp/index.php/client" data-icon="phone">Clients</a></li>
          <li><a href="http://crow1170.com/fieldapp/index.php/project" data-icon="bars" >Projects</a></li>
          <li><a href="http://crow1170.com/fieldapp/index.php/appointment" data-icon="location" >Appt.s</a></li>
        </ul>
      </div><!--/navbar-->
    </div><!--/header-->
<div data-role="content" data-theme="a">
<?php echo form_open($this->uri->uri_string()); ?>
		<span><?php echo form_label($login_label, $login['id']); ?></span>
		<span><?php echo form_input($login); ?></span>
		<span style="color: red;"><?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?></span>
		<span><?php echo form_label('Password', $password['id']); ?></span>
		<span><?php echo form_password($password); ?></span>
		<span style="color: red;"><?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?></span>
		<span colspan="3">
			<?php echo form_checkbox($remember); ?>
			<?php echo form_label('Remember me', $remember['id']); ?>
			<?php echo anchor('/auth/forgot_password/', 'Forgot password'); ?>
			<?php if ($this->config->item('allow_registration', 'tank_auth')) echo anchor('/auth/register/', 'Register'); ?>
		</span>
<?php echo form_submit('submit', 'Let me in'); ?>
<?php echo form_close(); ?>
</div></div></body></html>
