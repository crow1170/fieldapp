<div data-role="content">
  <form action="<?=current_url();?>" method="POST">
    <?php if(validation_errors()){?>
      <a href="#" data-role="button" data-theme="c" data-icon="alert">Client Name is required</a>
    <?php }else{ ?>
      <a href="#" data-role="button" data-theme="d" data-icon="edit">Creating New Client</a>
    <?php } ?>
    <label for="clientname" class="ui-hidden-accessible">Client Name:</label>
    <input type="text" name="clientname" id="clientname" placeholder="Client Name" value=""/>
    <label for="desc" class="ui-hidden-accessible">Client Description:</label>
    <textarea type="text" name="desc" id="desc" placeholder="Description"/></textarea>
    <label for="poc" class="ui-hidden-accessible">Point of Contact:</label>
    <textarea type="text" name="poc" id="poc" placeholder="Point of Contact"></textarea>
    <input type="submit" data-icon="user" value="Create New Client"/>
  </form>
</div><!--/content-->
