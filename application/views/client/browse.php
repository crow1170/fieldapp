<?php
  // Init
?>
<div data-role="content">
  <a href="<?=site_url('client/create');?>" data-role="button" data-theme="d" data-icon="plus">New Client</a>
<form class="ui-filterable"><input placeholder="Search..." id="filterBasic-input" data-type="search"></form>
<ul  class="ui-listview" data-role="listview" data-inset="true" data-filter="true" data-input="#filterBasic-input">
  <?php
    foreach($clients as $client){ ?>
      <li><a data-transition="flip" class="ui-btn ui-corner-all ui-shadow ui-btn-inline" href="<?=site_url('client/id').'/'.$client['clientId'];?>"><?=$client['name'];?></a></li>
    <?php }
  ?>
</ul>
</div><!--/content-->
