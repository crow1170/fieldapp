<?php
  // Init
?>
<div data-role="content">
  <a data-theme="a" data-role="button" data-icon="carat-l" href="<?=site_url('client').'/browse/';?>">Back to Browse</a>
  <h1><?=$client['name'];?></h1>
  <p><?=$client['desc'];?></p>
  <a data-theme="d" href="<?=site_url('project/create').'/'.$client['clientId'];?>" data-role="button" data-icon="plus">New Project</a>
  <a data-theme="b" data-role="button" data-icon="plus" href="<?=site_url('client/note').'/'.$client['clientId'];?>">New Note</a>
  <?=$projsHTML;?>
  <?=$notesHTML;?>
</div><!--/content-->
