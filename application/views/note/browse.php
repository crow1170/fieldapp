      <div data-role="collapsible">
        <h2>Notes</h2>
  <?php
    if(empty($notes)){ ?>
      <p><em>There are currently no notes on this object...</em></p>
    <?php }else{ ?>
        <ul style="display:inline;">
        <?php foreach($notes as $note){ ?>
          <li data-inset="false" data-iconpos="right" data-role="collapsible">
            <h3><?=$note['username'];?></h3>
            <div>
              <p style="white-space:normal;overflow:visible;"><?=$note['desc'];?></p>
            </div>
          </li>
        <?php } ?>
      </ul>
  <?php } ?>
    </div>
