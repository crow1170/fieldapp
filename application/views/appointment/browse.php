<?php
  // Init
?>
<div data-role="content">
  <a href="<?=site_url('appointment/create');?>" data-role="button" data-theme="d" data-icon="plus">New Appointment</a>
<form class="ui-filterable"><input placeholder="Search..." id="filter" data-type="search"></form>
<ul  class="ui-listview" data-role="listview" data-inset="true" data-filter="true" data-input="#filter">
  <?php
    foreach($appts as $appt){ ?>
      <li><a data-transition="flip" class="ui-btn ui-corner-all ui-shadow ui-btn-inline" href="<?=site_url('appointment/id').'/'.$appt['apptId'];?>"><?=$appt['date'];?></a></li>
    <?php }
  ?>
</ul>
</div><!--/content-->
