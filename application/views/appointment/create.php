<div data-role="content">
  <form action="<?=current_url();?>" method="POST">
    <?php if(validation_errors()){?>
      <a href="#" data-role="button" data-theme="c" data-icon="alert">Project is required</a>
    <?php }else{ ?>
      <a href="#" data-role="button" data-theme="d" data-icon="edit">Creating New Appointment</a>
    <?php } ?>
    <label for="clientId" class="ui-hidden-accessible">Project</label>
    <select name="projectId" id="projectId">
      <?php foreach($projects as $project){ ?>
        <option value="<?=$project['projId'];?>"><?=$project['name'];?></option>
      <?php } ?>
    </select>
    <label for="address" class="ui-hidden-accessible">Address:</label>
    <input type="text" name="address" id="address" placeholder="Address" value=""/>
    <label for="desc" class="ui-hidden-accessible">Appointment Description:</label>
    <textarea type="text" name="desc" id="desc" placeholder="Description"/></textarea>
    <label for="date">Date:</label>
    <input type="datetime-local" name="date" id="date" value="">
    <input type="submit" data-icon="clock" value="Create New Appointment"/>
  </form>
</div><!--/content-->
