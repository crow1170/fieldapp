<div data-role="content">
  <a data-theme="a" href="<?=site_url('project/id/').'/'.$appt['projId'];?>" data-role="button" data-icon="carat-l">Back to Project</a>
  <h1 id="timestamp"><?=date('D, F jS, g:ia', strtotime($appt['date']));?></h1>
  <a data-theme="d" href="<?=site_url('appointment/create').'/'.$appt['projId'];?>" data-role="button" data-icon="plus">New Appt</a>
  <a data-theme="c" data-role="button" data-icon="location" href="http://maps.google.com/maps?saddr=Current%20Location&daddr=<?=$appt['address'];?>">Directions</a>
  <a data-theme="b" data-role="button" data-icon="plus" href="<?=site_url('appointment/note').'/'.$appt['apptId'];?>">New Note</a>
  <br />
  <?=$notesHTML;?>
</div><!--/content-->
