<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Appointment extends CI_Controller
{
  function __construct()
  {
    parent::__construct();

    $this->load->helper('url');
    $this->load->library('tank_auth');
  }

  function index()
  {
    $this->browse();
  }
  function browse()
  {
    if (!$this->tank_auth->is_logged_in()) {
      redirect('/auth/login/');
    } else {
      $data['user_id']  = $this->tank_auth->get_user_id();
      $data['username']  = $this->tank_auth->get_username();
      $data['head']['tab'] = 'appt';
      $q = $this->db->query('SELECT * FROM `appointment`');
      $data['appts'] = $q->result_array();
      $this->load->view('templates/head', $data);
      $this->load->view('appointment/browse', $data);
      $this->load->view('templates/foot', $data);
    }
  }
  function id($apptId = null){
    if (!$this->tank_auth->is_logged_in()) {
      redirect('/auth/login/');
    } else {
      $data['user_id']  = $this->tank_auth->get_user_id();
      $data['username']  = $this->tank_auth->get_username();
      $data['head']['tab'] = 'appt';

      $q = $this->db->query('SELECT * FROM `appointment` WHERE `apptId`='.$apptId.' LIMIT 1');
      $data['appt'] = $q->result_array()[0];
      $q = $this->db->query('SELECT note.*, users.username FROM `note` INNER JOIN `users` ON note.userId=users.id WHERE note.parentType=\'a\' AND note.parentId='.$apptId);
      $data['notes'] = $q->result_array();

      $data['notesHTML'] = $this->load->view('note/browse', $data, TRUE);
      $this->load->view('templates/head', $data);
      $this->load->view('appointment/detail', $data);
      $this->load->view('templates/foot', $data);
    }
  }
  function create($projectId = 1){
    if (!$this->tank_auth->is_logged_in()) {
      redirect('/auth/login/');
    }else{
      $data['user_id']  = $this->tank_auth->get_user_id();
      $data['username']  = $this->tank_auth->get_username();
      $data['head']['tab'] = 'appt';
      $this->load->library('form_validation');
      $this->load->model('guide');
      $this->form_validation->set_rules('projectId', 'Project', 'required');
      if($this->form_validation->run() == FALSE){
        $data['projects'] = $this->guide->g('project');
        $this->load->view('templates/head', $data);
        $this->load->view('appointment/create', $data);
        $this->load->view('templates/foot', $data);
      }else{
        // Form Success
        $newAppointment = array(
          'date'=>$this->input->post('date', TRUE),
          'desc'=>$this->input->post('desc', TRUE),
          'address'=>$this->input->post('address', TRUE),
          'projId'=>$this->input->post('projectId', TRUE)
        );
        $apptId = $this->guide->i('appointment', $newAppointment);
        redirect(site_url('appointment/id').'/'.$apptId);
      }
    }
  }
  function note($parentId = 0){
    if (!$this->tank_auth->is_logged_in()) {
      redirect('/auth/login/');
    }else{
      $this->load->model('guide');
      $data['user_id']  = $this->tank_auth->get_user_id();
      $data['username']  = $this->tank_auth->get_username();
      $data['head']['tab'] = 'project';
      $this->load->library('form_validation');
      $this->form_validation->set_rules('desc', 'Note', 'required');
      if($this->form_validation->run() == FALSE){
        //$data['parent'] = $this->guide->g('appointment', array('projId'=>$parentId), TRUE);
        $this->load->view('templates/head', $data);
        $this->load->view('appointment/note', $data);
        $this->load->view('templates/foot', $data);
      }else{
        // Form Success
        $newNote = array(
          'desc'=>$this->input->post('desc', TRUE),
          'parentId'=>$parentId,
          'parentType'=>'a',
          'userId'=>$data['user_id']
        );
        $noteId = $this->guide->i('note', $newNote);
        redirect(site_url('appointment/id').'/'.$parentId);
      }
    }
  }
  function finish($apptId){
    if (!$this->tank_auth->is_logged_in()) {
      redirect('/auth/login/');
    } else {
      $data['user_id']  = $this->tank_auth->get_user_id();
      $data['username']  = $this->tank_auth->get_username();
      $data['head']['tab'] = 'appt';
      $this->load->view('templates/head', $data);
      $this->load->view('appointment/finish', $data);
      $this->load->view('templates/foot', $data);
    }
  }
}
