<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Client extends CI_Controller
{
  function __construct()
  {
    parent::__construct();

    $this->load->helper('url');
    $this->load->library('tank_auth');
  }

  function index()
  {
    $this->browse();
  }
  function browse()
  {
    if (!$this->tank_auth->is_logged_in()) {
      redirect('/auth/login/');
    } else {
      $data['user_id']  = $this->tank_auth->get_user_id();
      $data['username']  = $this->tank_auth->get_username();
      $data['head']['tab'] = 'client';
      $q = $this->db->query('SELECT `name`, `clientId`, `desc`, `photo` FROM `client` WHERE 1');
      $data['clients'] = $q->result_array();
      $this->load->view('templates/head', $data);
      $this->load->view('client/browse', $data);
      $this->load->view('templates/foot', $data);
    }
  }
  function id($clientId = 1){
    if (!$this->tank_auth->is_logged_in()) {
      redirect('/auth/login/');
    } else {
      $db['clients']['0']  = array('Error', 'Error');
      $q = $this->db->query('SELECT `name`, `clientId`, `desc`, `photo` FROM `client` WHERE `clientId`='.$clientId.' LIMIT 1');
      $data['client'] = $q->result_array()[0];
      $q = $this->db->query('SELECT * FROM `project` WHERE `clientId`='.$clientId);
      $data['projects'] = $q->result_array();
      $q = $this->db->query('SELECT note.*, users.username FROM `note` INNER JOIN `users` ON note.userId=users.id WHERE note.parentType=\'c\' AND note.parentId='.$clientId);
      $data['notes'] = $q->result_array();

      $data['user_id']  = $this->tank_auth->get_user_id();
      $data['username']  = $this->tank_auth->get_username();
      $data['head']['tab'] = 'client';
      $data['projsHTML'] = $this->load->view('child/project', $data, TRUE);
      $data['notesHTML'] = $this->load->view('note/browse', $data, TRUE);
      $this->load->view('templates/head', $data);
      $this->load->view('client/detail', $data);
      $this->load->view('templates/foot', $data);
    }
  }
  function create(){
    if (!$this->tank_auth->is_logged_in()) {
      redirect('/auth/login/');
    }else{
      $data['user_id']  = $this->tank_auth->get_user_id();
      $data['username']  = $this->tank_auth->get_username();
      $data['head']['tab'] = 'client';
      $this->load->library('form_validation');
      $this->form_validation->set_rules('clientname', 'Client Name', 'required');
      if($this->form_validation->run() == FALSE){
        $this->load->view('templates/head', $data);
        $this->load->view('client/create', $data);
        $this->load->view('templates/foot', $data);
      }else{
        // Form Success
        $this->load->model('guide');
        $newClient = array(
          'name'=>$this->input->post('clientname', TRUE),
          'desc'=>$this->input->post('desc', TRUE),
          'photo'=>'http://www.clker.com/cliparts/Z/J/g/U/V/b/avatar-male-silhouette-md.png',
          'poc'=>$this->input->post('poc', TRUE),
          'employee'=>$data['user_id']
        );
        $clientId = $this->guide->i('client', $newClient);
        redirect(site_url('client/id').'/'.$clientId);
      }
    }
  }
  function note($clientId = 0){
    if (!$this->tank_auth->is_logged_in()) {
      redirect('/auth/login/');
    }else{
      $this->load->model('guide');
      $data['user_id']  = $this->tank_auth->get_user_id();
      $data['username']  = $this->tank_auth->get_username();
      $data['head']['tab'] = 'client';
      $this->load->library('form_validation');
      $this->form_validation->set_rules('desc', 'Note', 'required');
      if($this->form_validation->run() == FALSE){
        $data['client'] = $this->guide->g('client', array('clientId'=>$clientId), TRUE);
        $this->load->view('templates/head', $data);
        $this->load->view('client/note', $data);
        $this->load->view('templates/foot', $data);
      }else{
        // Form Success
        $newNote = array(
          'desc'=>$this->input->post('desc', TRUE),
          'parentId'=>$clientId,
          'parentType'=>'c',
          'userId'=>$data['user_id']
        );
        $noteId = $this->guide->i('note', $newNote);
        redirect(site_url('client/id').'/'.$clientId);
      }
    }
  }
}
