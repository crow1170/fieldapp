<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Project extends CI_Controller
{
  function __construct()
  {
    parent::__construct();

    $this->load->helper('url');
    $this->load->library('tank_auth');
  }

  function index()
  {
    $this->browse();
  }
  function browse()
  {
    if (!$this->tank_auth->is_logged_in()) {
      redirect('/auth/login/');
    } else {
      $data['user_id']  = $this->tank_auth->get_user_id();
      $data['username']  = $this->tank_auth->get_username();
      $q = $this->db->query('SELECT * FROM `project`');
      $data['projects'] = $q->result_array();
      $data['head']['tab'] = 'proj';
      $this->load->view('templates/head', $data);
      $this->load->view('project/browse', $data);
      $this->load->view('templates/foot', $data);
    }
  }
  function id($projectId = 1){
    if (!$this->tank_auth->is_logged_in()) {
      redirect('/auth/login/');
    } else {
      // GET proj
      $q = $this->db->query('SELECT `name`, `clientId`, `desc`, `projId` FROM `project` WHERE `projId`='.$projectId.' LIMIT 1');
      $data['project'] = $q->result_array()[0];
      //GET appts
      $q = $this->db->query('SELECT * FROM `appointment` WHERE `projId`='.$projectId);
      $data['appts'] = $q->result_array();
      //GET notes
      $q = $this->db->query('SELECT note.*, users.username FROM `note` INNER JOIN `users` ON note.userId=users.id WHERE note.parentType=\'p\' AND note.parentId='.$projectId);
      $data['notes'] = $q->result_array();

      $data['user_id']  = $this->tank_auth->get_user_id();
      $data['username']  = $this->tank_auth->get_username();
      $data['head']['tab'] = 'proj';
      $data['notesHTML'] = $this->load->view('note/browse', $data, TRUE);

      $this->load->view('templates/head', $data);
      $this->load->view('project/detail', $data);
      $this->load->view('templates/foot', $data);
    }
  }
  function create($clientId = 0){
    if (!$this->tank_auth->is_logged_in()) {
      redirect('/auth/login/');
    }else{
      $data['user_id']  = $this->tank_auth->get_user_id();
      $data['username']  = $this->tank_auth->get_username();
      $data['head']['tab'] = 'proj';
      $this->load->library('form_validation');
      $this->load->model('guide');
      $this->form_validation->set_rules('projectname', 'Project Name', 'required');
      if($this->form_validation->run() == FALSE){
        $data['clients'] = $this->guide->g('client');
        $this->load->view('templates/head', $data);
        $this->load->view('project/create', $data);
        $this->load->view('templates/foot', $data);
      }else{
        // Form Success
        $newProject = array(
          'name'=>$this->input->post('projectname', TRUE),
          'desc'=>$this->input->post('desc', TRUE),
          'clientId'=>$this->input->post('clientId', TRUE)
        );
        $projectId = $this->guide->i('project', $newProject);
        redirect(site_url('project/id').'/'.$projectId);
      }
    }
  }
  function note($parentId = 0){
    if (!$this->tank_auth->is_logged_in()) {
      redirect('/auth/login/');
    }else{
      $this->load->model('guide');
      $data['user_id']  = $this->tank_auth->get_user_id();
      $data['username']  = $this->tank_auth->get_username();
      $data['head']['tab'] = 'project';
      $this->load->library('form_validation');
      $this->form_validation->set_rules('desc', 'Note', 'required');
      if($this->form_validation->run() == FALSE){
        $data['parent'] = $this->guide->g('project', array('projId'=>$parentId), TRUE);
        $this->load->view('templates/head', $data);
        $this->load->view('project/note', $data);
        $this->load->view('templates/foot', $data);
      }else{
        // Form Success
        $newNote = array(
          'desc'=>$this->input->post('desc', TRUE),
          'parentId'=>$parentId,
          'parentType'=>'p',
          'userId'=>$data['user_id']
        );
        $noteId = $this->guide->i('note', $newNote);
        redirect(site_url('project/id').'/'.$parentId);
      }
    }
  }
}
