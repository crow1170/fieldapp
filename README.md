# FieldApp #

This CRM tool lets organizations keep track of important data anytime and anyplace.

v1.0.0

The intent of FieldApp is to get Johnny Newhire up to speed in just a few minutes. Send him the link to the appointment he'll be going to, and from it he'll know what he's supposed to do, what the goal of the project is, and what interactions you've had with the client in the past.

### How do I get set up? ###

To deploy FieldApp, you'll need a webserver with php and a database

* Configuration
Once you've cloned the repository, import `/schema.sql` into your database. Edit `/application/config/config.php`, `/application/config/database.php`, and `/application/config/tank_auth.php`, making particularly sure to:
  * Improve your encryption key
  * Set up your database, host, db user, and db password
  * Change your brand/project name

### How do I use it? ###

Once you've deployed FieldApp, 

* Register users from `your-host.com/fieldapp/index.php/auth/register`
* Create new clients from `your-host.com/fieldapp/index.php/client`
* Get a detailed look at those clients and create projects for them
* Create appointments for those projects
* Make notes on anything

### Roadmap ###

Here's what we intend for FieldApp:
u
* Edit features
  Users should be able to fix typos, revise descriptions and reschedule appoinments
* Full depth search
  Users should be able to find the mention of any word in any object- be they clients, projects, appointments, or (most importantly) notes
* Importing
  Clients are often already stored in vCards or.csvs- users should be able to upload them and import them into the database
* Notifications
  Users should be able to see which objects have been added or updated since they last signed in
* Checkin
  Users should be able to easily log time spent at an appointment and notify their team

### Who do I talk to? ###

FieldApp is being developed by [Chris Knowles](http://crow1170.com). If you'd like to contribute, congratulate, or complain you can reach him via [email](cknowles117@gmail.com).